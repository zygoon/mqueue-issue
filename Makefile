CFLAGS = -Wall -g
.PHONY: all clean run-unconfined run-with-30 run-with-40
all:: run-unconfined
all:: run-with-30
all:: run-with-40
clean:
	rm -f mqueue main.o
	rm -f x-test-mqueue.apparmor.30.bin x-test-mqueue.apparmor.40.bin
run-unconfined: mqueue
	./$<
run-with-30: x-test-mqueue.apparmor.30.bin mqueue
	-sudo apparmor_parser \
		--remove \
		--verbose \
		x-test-mqueue.apparmor 
	sudo apparmor_parser \
		--skip-cache \
		--verbose \
		--binary $<
	aa-exec -p x-test-mqueue ./mqueue
run-with-40: x-test-mqueue.apparmor.40.bin mqueue 
	-sudo apparmor_parser \
		--remove \
		--verbose \
		x-test-mqueue.apparmor 
	sudo apparmor_parser \
		--skip-cache \
		--verbose \
		--binary $<
	aa-exec -p x-test-mqueue ./mqueue
mqueue: LDFLAGS=-lrt
mqueue: main.o
	$(LINK.o) $(OUTPUT_OPTION) $<
x-test-mqueue.apparmor.30.bin: x-test-mqueue.apparmor 
	/snap/snapd/current/usr/lib/snapd/apparmor_parser \
		--base /snap/snapd/current/usr/lib/snapd/apparmor.d \
		--config-file /snap/snapd/current/usr/lib/snapd/apparmor/parser.conf \
		--policy-features /snap/snapd/current/usr/lib/snapd/apparmor.d/abi/3.0 \
		--skip-cache \
		--warn all \
		--skip-kernel-load \
		--ofile $@ $<
x-test-mqueue.apparmor.40.bin: x-test-mqueue.apparmor
	apparmor_parser \
		--policy-features /etc/apparmor.d/abi/4.0 \
		--skip-cache \
		--warn all \
		--skip-kernel-load \
		--ofile $@ $<
