#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <mqueue.h>
#include <sys/stat.h>

#include <errno.h>
#include <unistd.h>

int main(void) {
  const char *name = "/foo";
  struct mq_attr attrs = {
      .mq_maxmsg = 10,
      .mq_msgsize = 10,
  };
  struct stat stat_buf;
  if (lstat("/dev/mqueue", &stat_buf) < 0) {
    perror("stat /dev/mqueue");
    return errno;
  }
  if (mq_unlink(name) == -1 && errno != ENOENT) {
    perror("mq_unlink");
    return errno;
  }
  mqd_t mq = mq_open(name, O_RDWR | O_CREAT, 0666, &attrs);
  if (mq == -1) {
    perror("mq_open");
    return errno;
  }
  if (mq_close(mq) == -1) {
    perror("mq_close");
    return errno;
  }
  if (mq_unlink(name) == -1) {
    perror("mq_unlink");
    return errno;
  }
  printf("ALL GOOD\n");
  return 0;
}
